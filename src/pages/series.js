import React from 'react';
import Helper from '../utills/helper';
import Api from '../services/network/api';
import { request } from '../services/network/router';
import '../styles/global.css';
import Loader from '../components/loader';

class Series extends React.Component {
  state = {
    results: undefined
  };

  getApi = id => {
    const { ts, hash } = Helper.paramHelper();
    this.getSeries(id, ts, hash);
  };

  getSeries = async (id, ts, hash) => {
    const param = {
      api: Api.getById,
      part: 'series',
      id,
      ts,
      hash
    };
    const response = await request(param);
    console.log(response);
    const { data } = response;
    const { results } = data;
    this.setState({
      results
    });
  };

  componentWillMount() {
    const { id } = this.props.match.params;
    console.log(id);
    this.getApi(id);
  }

  render() {
    const { results } = this.state;
    console.log(results);
    return (
      <div>
        {results && (
          <div>
            <div className="title" style={{ textAlign: 'center', margin: 12 }}>
              Marvel Series
            </div>
            {results &&
              results.map((item, index) => {
                const { thumbnail, title, urls } = item;
                return (
                  <div>
                    <div key={index} style={{ margin: 12, display: 'flex' }}>
                      <img
                        alt=""
                        src={`${thumbnail.path}.${thumbnail.extension}`}
                        style={{ width: 150 }}
                      />
                      <div style={{ margin: 12 }}>
                        <div style={{ fontFamily: 'Lato', padding: 12 }}>
                          {title}
                        </div>
                        {urls &&
                          urls.map((value, index) => {
                            const { type, url } = value;
                            return (
                              <div>
                                <button
                                  className="outlined-button"
                                  style={{
                                    margin: 12,
                                    padding: 12,
                                    width: 120,
                                    fontFamily: 'Lato',
                                    textTransform: 'uppercase',
                                    fontWeight: 'bold'
                                  }}
                                  onClick={() => window.open(url, '_blank')}
                                >
                                  {type}
                                </button>
                              </div>
                            );
                          })}
                      </div>
                    </div>
                    <hr />
                  </div>
                );
              })}
          </div>
        )}
        {results === undefined && <Loader />}
      </div>
    );
  }
}

export default Series;
