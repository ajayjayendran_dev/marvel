import React, { Fragment } from 'react';
import Helper from '../utills/helper';
import Api from '../services/network/api';
import { request } from '../services/network/router';
import Loader from '../components/loader';

class Stories extends React.Component {
  state = {
    results: undefined
  };

  getApi = id => {
    const { ts, hash } = Helper.paramHelper();
    this.getSeries(id, ts, hash);
  };

  getSeries = async (id, ts, hash) => {
    const param = {
      api: Api.getById,
      part: 'stories',
      id,
      ts,
      hash
    };
    const response = await request(param);
    const { data } = response;
    const { results } = data;
    this.setState({
      results
    });
  };

  componentWillMount() {
    const { id } = this.props.match.params;
    this.getApi(id);
  }

  render() {
    const { results } = this.state;
    return (
      <div>
        {results && (
          <Fragment>
            <div className="title" style={{ textAlign: 'center', margin: 12 }}>
              Marvel Stories
            </div>
            {results &&
              results.map((item, index) => {
                const { creators, title, type } = item;
                const { items } = creators;
                return (
                  <div key={index} style={{ margin: 12 }}>
                    {items && items.length > 0 && (
                      <Fragment>
                        <div>
                          <div className="title">{title}</div>
                          <div
                            style={{
                              fontWeight: 'bold',
                              fontFamily: 'Lato',
                              color: '#9a9a9a'
                            }}
                          >
                            Type: {type}
                          </div>
                          <div className="creatorsContainer">
                            {items.map(creator => {
                              const { name, role } = creator;
                              return (
                                <div
                                  style={{
                                    boxShadow:
                                      'rgba(0, 0, 0, 0.08) 0px 4px 8px 0px',
                                    padding: 24,
                                    margin: 12
                                  }}
                                >
                                  <div className="title">{name}</div>
                                  <div className="highlighter">{role}</div>
                                </div>
                              );
                            })}
                          </div>
                        </div>
                        <hr />
                      </Fragment>
                    )}
                  </div>
                );
              })}
          </Fragment>
        )}
        {results === undefined && <Loader />}
      </div>
    );
  }
}

export default Stories;
