import React, { Fragment } from 'react';
import Helper from '../utills/helper';
import Api from '../services/network/api';
import { request } from '../services/network/router';
import Card from '../components/card';
import Loader from '../components/loader';

class Comics extends React.Component {
  state = {
    results: undefined
  };

  getApi = id => {
    const { ts, hash } = Helper.paramHelper();
    this.getComics(id, ts, hash);
  };

  getComics = async (id, ts, hash) => {
    const param = {
      api: Api.getById,
      part: 'comics',
      id,
      ts,
      hash
    };
    const response = await request(param);
    console.log(response);
    const { data } = response;
    const { results } = data;
    this.setState({
      results
    });
  };

  componentWillMount() {
    const { id } = this.props.match.params;
    console.log(id);
    this.getApi(id);
  }

  render() {
    const { results } = this.state;
    console.log(results);
    return (
      <div>
        {results && (
          <div>
            <div className="title" style={{ textAlign: 'center', margin: 12 }}>
              Marvel Comics
            </div>
            {results &&
              results.map((item, index) => {
                const { images, title, description, prices } = item;
                return (
                  <div key={index} style={{ margin: 12 }}>
                    {images && images.length > 0 && (
                      <Fragment>
                        <Card
                          image={images[0]}
                          title={title}
                          description={description}
                          prices={prices}
                        />
                        <hr />
                      </Fragment>
                    )}
                  </div>
                );
              })}
          </div>
        )}
        {results === undefined && <Loader />}
      </div>
    );
  }
}

export default Comics;
