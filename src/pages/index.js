import React, { Fragment } from 'react';
import Helper from '../utills/helper';
import Api from '../services/network/api';
import { request } from '../services/network/router';
import FlipCard from '../components/flipcard';
import Loader from '../components/loader';

class Home extends React.Component {
  state = {
    results: undefined
  };

  getApi = () => {
    const { ts, hash } = Helper.paramHelper();
    this.getCharacters(ts, hash);
  };

  getCharacters = async (ts, hash) => {
    const param = {
      api: Api.getCharacters,
      type: 'characters',
      ts,
      hash
    };
    const characters = JSON.parse(localStorage.getItem('characters') || '[]');
    if (characters.length === 0) {
      const response = await request(param);
      const { data } = response;
      const { results } = data;
      localStorage.setItem('characters', JSON.stringify(results));
      this.setState({
        results
      });
    } else if (characters.length > 0) {
      this.setState({
        results: characters
      });
    }
  };

  componentWillMount() {
    this.getApi();
  }

  render() {
    const { results } = this.state;
    return (
      <div>
        {results && (
          <Fragment>
            <div className="title" style={{ textAlign: 'center', margin: 12 }}>
              Marvel Characters
            </div>
            <div
              style={{
                display: 'flex',
                flexWrap: 'wrap',
                justifyContent: 'center'
              }}
            >
              {results &&
                results.map((item, index) => {
                  const { name, thumbnail, description, id } = item;
                  return (
                    <div key={index}>
                      <FlipCard
                        name={name}
                        image={thumbnail}
                        id={id}
                        description={
                          description !== ''
                            ? description
                            : `Marvel doesn't provide description for ${name}`
                        }
                      />
                    </div>
                  );
                })}
            </div>
          </Fragment>
        )}
        {results === undefined && <Loader />}
      </div>
    );
  }
}

export default Home;
