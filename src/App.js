import React from 'react';
import { Route, Switch, Router } from 'react-router-dom';
import Home from './pages';
import { createBrowserHistory } from 'history';
import Comics from './pages/comics';
import Series from './pages/series';
import Stories from './pages/stories';
import Logo from './assets/logo.png';
import './styles/global.css';
export const history = createBrowserHistory();
class App extends React.Component {
  state = {
    characters: undefined
  };

  render() {
    return (
      <div>
        <img
          src={Logo}
          alt="marvel"
          style={{ width: 150 }}
          onClick={() => (window.location.href = '/')}
        />
        <div style={{ textAlign: 'center' }} className="title">
          Hey There, Explore Marvel Characters, Comics,Stories, Series
        </div>
        <Router history={history}>
          <Switch>
            <Route path="/" component={Home} exact />
            <Route path="/:id/comics" component={Comics} />
            <Route path="/:id/series" component={Series} />
            <Route path="/:id/stories" component={Stories} />
          </Switch>
        </Router>
      </div>
    );
  }
}

export default App;
