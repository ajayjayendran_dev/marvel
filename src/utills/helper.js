import crypto from 'crypto';
import Apiconstant from '../services/Apiconstant';

const paramHelper = () => {
  const ts = new Date().getTime();
  const hash = crypto
    .createHash('md5')
    .update(ts + Apiconstant.PRIVATE_KEY + Apiconstant.PUBLIC_KEY)
    .digest('hex');
  return { ts, hash };
};

export default {
  paramHelper
};
