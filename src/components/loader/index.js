import React from 'react';
import loader from '../../assets/loader.gif';
import '../../styles/global.css';

function Loader(props) {
  return (
    <div className="loader">
      <img src={loader} alt="" />
    </div>
  );
}

export default Loader;
