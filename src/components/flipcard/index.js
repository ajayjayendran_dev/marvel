import React from 'react';
import './index.css';
import '../../styles/global.css';

const Front = props => {
  const { name, image } = props;

  return (
    <div className="front">
      <img
        alt=""
        style={{ background: '#123', width: '100%', height: 300 }}
        src={`${image.path}.${image.extension}`}
      />
      <div className="title">{name}</div>
    </div>
  );
};

const Back = props => {
  const { description, callComics, callSeries, callStories } = props;
  return (
    <div className="back">
      <div className="description">{description}</div>
      <button
        onClick={callComics}
        style={{
          padding: 12,
          margin: 6
        }}
        className="outlined-button"
      >
        Comics
      </button>
      <button
        onClick={callSeries}
        style={{
          padding: 12,
          margin: 6
        }}
        className="outlined-button"
      >
        Series
      </button>
      <button
        onClick={callStories}
        style={{
          padding: 12,
          margin: 6
        }}
        className="outlined-button"
      >
        Stories
      </button>
    </div>
  );
};

class FlipCard extends React.Component {
  state = {
    flipped: false
  };

  flip = () => {
    this.setState({ flipped: !this.state.flipped });
  };

  callComics = () => {
    const { id } = this.props;
    window.location.href = `/${id}/comics`;
  };

  callSeries = () => {
    const { id } = this.props;
    window.location.href = `/${id}/series`;
  };

  callStories = () => {
    const { id } = this.props;
    window.location.href = `/${id}/stories`;
  };

  render() {
    const { flipped } = this.state;
    const { name, image, description } = this.props;
    return (
      <div
        onMouseEnter={this.flip}
        onMouseLeave={this.flip}
        className={'card-container' + (flipped ? ' flipped' : '')}
      >
        <Front name={name} image={image} />
        <Back
          description={description}
          callComics={this.callComics}
          callSeries={this.callSeries}
          callStories={this.callStories}
        />
      </div>
    );
  }
}

export default FlipCard;
