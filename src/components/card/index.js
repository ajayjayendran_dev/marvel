import React from 'react';

const Card = props => {
  const { title, description, prices, image } = props;
  return (
    <div style={{ display: 'flex', alignItems: 'center' }}>
      {image && (
        <img
          alt=""
          src={`${image.path}.${image.extension}`}
          style={{ width: 150 }}
        />
      )}
      <div style={{ maxWidth: 600, marginLeft: 30 }}>
        <div style={{ fontWeight: 'bold', fontFamily: 'Lato', padding: 6 }}>
          {title}
        </div>
        <div
          dangerouslySetInnerHTML={{ __html: description }}
          style={{ fontFamily: 'Muli', padding: 6 }}
        />
        {prices.map((item, index) => {
          const { type, price } = item;
          return (
            <div key={index} style={{ display: 'flex', padding: 6 }}>
              <div style={{ textTransform: 'capitalize', fontFamily: 'Muli' }}>
                {type} :{' '}
              </div>
              <div style={{ fontFamily: 'Lato' }}>${price}</div>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default Card;
