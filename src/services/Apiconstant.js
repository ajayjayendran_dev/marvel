export default {
  PUBLIC_KEY: 'a452b605e3f6de90731d27b1e92623aa',
  PRIVATE_KEY: '0ac42e7ab75230ca4116432f260b8e8764d28eb9',
  BASE_URL: 'https://gateway.marvel.com',
  PORT: ':443/',
  V1: 'v1/',
  PUBLIC: 'public/',
  CHARACTER: 'characters',
  CREATORS: 'creators',
  SERIES: 'series',
  COMICS: 'comics',
  APIKEY: 'apikey',
  GET: 'get'
};
