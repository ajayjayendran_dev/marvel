import Apiconstant from '../Apiconstant';

export default {
  getCharacters: {
    url: `${Apiconstant.BASE_URL}${Apiconstant.PORT}${Apiconstant.V1}${Apiconstant.PUBLIC}${Apiconstant.CHARACTER}?${Apiconstant.APIKEY}=${Apiconstant.PUBLIC_KEY}`,
    method: Apiconstant.GET
  },
  getSeries: {
    url: `${Apiconstant.BASE_URL}${Apiconstant.PORT}${Apiconstant.V1}${Apiconstant.PUBLIC}${Apiconstant.SERIES}?${Apiconstant.APIKEY}=${Apiconstant.PUBLIC_KEY}`,
    method: Apiconstant.GET
  },
  getComics: {
    url: `${Apiconstant.BASE_URL}${Apiconstant.PORT}${Apiconstant.V1}${Apiconstant.PUBLIC}${Apiconstant.COMICS}?${Apiconstant.APIKEY}=${Apiconstant.PUBLIC_KEY}`,
    method: Apiconstant.GET
  },
  getCreators: {
    url: `${Apiconstant.BASE_URL}${Apiconstant.PORT}${Apiconstant.V1}${Apiconstant.PUBLIC}${Apiconstant.CREATORS}?${Apiconstant.APIKEY}=${Apiconstant.PUBLIC_KEY}`,
    method: Apiconstant.GET
  },
  getById: {
    url: `${Apiconstant.BASE_URL}${Apiconstant.PORT}${Apiconstant.V1}${Apiconstant.PUBLIC}${Apiconstant.CHARACTER}`,
    method: Apiconstant.GET
  }
};
