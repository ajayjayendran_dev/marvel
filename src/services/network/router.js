import Axios from 'axios';
import ApiConstant from '../Apiconstant';

export const request = params => {
  if (params.type === 'characters') {
    return Axios({
      url: `${params.api.url}&ts=${params.ts}&hash=${params.hash}`,
      method: params.api.method,
      params: params.data
    })
      .then(response => response.data)
      .catch(err => console.log(err));
  }
  return Axios({
    url: `${params.api.url}/${params.id}/${params.part}?apikey=${ApiConstant.PUBLIC_KEY}&ts=${params.ts}&hash=${params.hash}`,
    method: params.api.method,
    params: params.data
  })
    .then(response => response.data)
    .catch(err => console.log(err));
};
